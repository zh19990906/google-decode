# -*- coding: utf-8 -*-
# @Time : 2024/1/11 11:00
# @Author : ZH
# @File : setup.py
# @Software: PyCharm
from setuptools import setup, find_packages

setup(
    name='google_decode',
    version='0.2',
    packages=find_packages(),
    install_requires=[
        'numpy>=1.21.6',
        'requests>=2.28.1'
    ],
)
